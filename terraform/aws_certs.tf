resource "aws_acm_certificate" "acm_certificate_argo" {
  domain_name               = "argo-${var.enviroment}.${var.domain}"
  validation_method         = "DNS"
  tags = {
    "Environment" = "${var.enviroment}"
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate" "acm_certificate_api_application" {
  domain_name               = "api-application-${var.enviroment}.${var.domain}"
  validation_method         = "DNS"
  tags = {
    "Environment" = "${var.enviroment}"
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate" "acm_certificate_static_application" {
  domain_name               = "static-application-${var.enviroment}.${var.domain}"
  validation_method         = "DNS"
  tags = {
    "Environment" = "${var.enviroment}"
  }
  lifecycle {
    create_before_destroy = true
  }
}