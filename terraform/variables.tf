variable "aws_region" {
  type        = string
  description = "AWS region"
  default     = "us-east-1"
}

variable "key-test-volindo" {
  description = "uri file key pub"
  type        = string
  default     = "./resources/ssh/KeyTestMarfeel.pub" #change for tou public key to acces server
}

variable "cluster_name" {
  type    = string
  default = "volindo-test"
}

variable "enviroment" {
  type    = string
  default = "develop"
}

variable "cluster_version" {
  type    = string
  default = "1.26" # chose your version of k8s
}

variable "argo_cd_version" {
  type    = string
  default = "5.37.0" # Versión de Argo CD
}

variable "app_name" {
  type        = string
  description = "Application Name"
  default     = "volindo-test" 
}

variable "domain" {
  type        = string
  description = "set your domain"
  default     = "ihacktec.com" 
}

## Future feature using cert-manager
variable "email" {
  type        = string
  description = "email for letsencrypt"
  default     = "angel@ihacktec.com"
}