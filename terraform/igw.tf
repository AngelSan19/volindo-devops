resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.aws-vpc-volindo-test.id

  tags = {
    Name = "igw-${var.app_name}-${var.enviroment}"
  }
}