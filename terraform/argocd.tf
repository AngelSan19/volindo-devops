data "external" "argo_token" {
  program = ["bash", "./resources/get_argo_token.sh","${var.cluster_name}", "${var.enviroment}", "${var.aws_region}"]
}


provider "argocd" {
  server_addr = "argo-${var.enviroment}.${var.domain}"
  username    = "admin"
  password    = data.external.argo_token.result.token
}


resource "argocd_application" "api-application" {
  metadata {
    name      = "api-application"
    namespace = "argocd"
  }

  spec {
    project = "default"


    source {
      repo_url        = "https://gitlab.com/AngelSan19/volindo-devops.git"
      path            = "api-application/k8s"
      target_revision = var.enviroment
      ref             = "values"
    }

    sync_policy {
      automated {
        prune       = true
        self_heal   = true
        allow_empty = true
      }
      sync_options = ["Validate=false", "ApplyOutOfSyncOnly=true", "CreateNamespace=true"]
    }

    destination {
      server    = "https://kubernetes.default.svc"
      namespace = var.enviroment
    }
  }
  depends_on = [helm_release.argo_cd,data.external.argo_token]
}

resource "argocd_application" "static-application" {
  metadata {
    name      = "static-application"
    namespace = "argocd"
  }

  spec {
    project = "default"


    source {
      repo_url        = "https://gitlab.com/AngelSan19/volindo-devops.git"
      path            = "static-application/k8s"
      target_revision = "${var.enviroment}"
      ref             = "values"
    }

    sync_policy {
      automated {
        prune       = true
        self_heal   = true
        allow_empty = true
      }
      sync_options = ["Validate=false","ApplyOutOfSyncOnly=true","CreateNamespace=true"]
    }

    destination {
      server    = "https://kubernetes.default.svc"
      namespace = "${var.enviroment}"
    }
  }
  depends_on = [helm_release.argo_cd,data.external.argo_token]
}
