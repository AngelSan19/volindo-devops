provider "helm" {
  kubernetes {
    host                   = aws_eks_cluster.cluster.endpoint
    cluster_ca_certificate = base64decode(aws_eks_cluster.cluster.certificate_authority[0].data)
    exec {
      api_version = "client.authentication.k8s.io/v1"
      args        = ["eks", "get-token", "--cluster-name", aws_eks_cluster.cluster.id]
      command     = "aws"
    }
  }
}

## Posible feature
resource "helm_release" "cert_manager" {
  name       = "cert-manager-letsencrypt"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "v1.12.2"
  namespace  = "cert-manager"
  create_namespace = true

  set {
    name  = "installCRDs"
    value = "true"
  }

  set {
    name  = "enableCertificateOwnerRef"
    value = "false"
  }

  set {
    name  = "global.rbac.create"
    value = "true"
  }

  set {
    name  = "extraArgs[0]"
    value = "clusterIssuers[0].name=letsencrypt-${var.enviroment}"
  }

  set {
    name  = "extraArgs[1]"
    value = "clusterIssuers[0].acme.server=https://acme-staging-v02.api.letsencrypt.org/directory"
  }

  set {
    name  = "extraArgs[2]"
    value = "clusterIssuers[0].acme.email=${var.email}"
  }

  set {
    name  = "extraArgs[3]"
    value = "clusterIssuers[0].acme.privateKeySecretRef.name=letsencrypt-${var.enviroment}"
  }

  set {
    name  = "extraArgs[4]"
    value = "clusterIssuers[0].acme.solvers[0].http01.ingress.class=nginx"
  }

# set {
#   name  = "extraArgs[5]"
#   value = "controller-args[0]=--controllers=*,-certificaterequests-approver"
# }

}




resource "helm_release" "aws-load-balancer-controller" {
  name = "aws-load-balancer-controller-${var.app_name}-${var.enviroment}"

  repository = "https://aws.github.io/eks-charts"
  chart      = "aws-load-balancer-controller"
  namespace  = "kube-system"
  version    = "1.4.1"

 
  set {
    name  = "clusterName"
    value = aws_eks_cluster.cluster.id
  }

  set {
    name  = "image.tag"
    value = "v2.4.2"
  }

  set {
    name  = "serviceAccount.name"
    value = "aws-load-balancer-controller"
  }

  set {
    name  = "serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn"
    value = aws_iam_role.aws_load_balancer_controller.arn
  }

  depends_on = [
    aws_eks_node_group.private-nodes,
    aws_iam_role_policy_attachment.aws_load_balancer_controller_attach,
  ]
}

resource "helm_release" "argo_cd" {
  name             = "argo-cd-${var.app_name}-${var.enviroment}"
  repository       = "https://argoproj.github.io/argo-helm"
  chart            = "argo-cd"
  version          = var.argo_cd_version
  namespace        = "argocd"
  create_namespace = true

  set {
    name  = "configs.params.server\\.insecure"
    value = "true"
  }

  set {
    name  = "server.ingress.enabled"
    value = "true"
  }

  # set {
  #   name  = "server.ingress.annotations.alb.\\.ingress\\.kubernetes\\.io/certificate-arn"
  #   value = aws_acm_certificate.acm_certificate_argo.arn
  # }

  # set {
  #   name  = "server.ingress.annotations.kubernetes\\.io/ingress\\.class"
  #   value = "nginx"
  # }

  set {
    name  = "server.ingress.annotations.cert-manager\\.io/issuer"
    value = "letsencrypt-${var.enviroment}"
  }

  set {
    name  = "server.ingress.annotations.alb\\.ingress\\.kubernetes\\.io/rewrite-target"
    value = "/"
  }

  set {
    name  = "server.ingress.annotations.alb\\.ingress\\.kubernetes\\.io/scheme"
    value = "internet-facing"
  }

  set {
    name  = "server.ingress.annotations.alb\\.ingress\\.kubernetes\\.io/group\\.name"
    value = "develop"
  }

  set {
    name  = "server.ingress.annotations.alb\\.ingress\\.kubernetes\\.io/group\\.order"
    value = "1"
  }
  
  set {
    name  = "server.ingress.annotations.alb\\.ingress\\.kubernetes\\.io/listen-ports"
    value = "[{\"HTTPS\": 443}]" # add port 80 but i have probles with the syntax
  }
  set {
    name  = "server.ingress.ingressClassName"
    value = "alb"
  }

  set {
    name  = "server.ingress.hosts[0]"
    value = "argo-${var.enviroment}.${var.domain}"
  }

  set {
    name  = "server.tls[0].hosts[0]"
    value = "argo-${var.enviroment}.${var.domain}"
  }

  set {
    name  = "server.tls[0].secretName"
    value = "argo-${var.enviroment}.${var.domain}"
  }

  set {
    name  = "server.ingress.https"
    value = "true"
  }

  set {
    name  = "server.config.tlsClientConfig.insecure"
    value = "true"
  }

  set {
    name  = "server.service.type"
    value = "NodePort"
  }

  depends_on = [
    aws_eks_node_group.private-nodes,
    aws_iam_role_policy_attachment.aws_load_balancer_controller_attach,
  ]
}
