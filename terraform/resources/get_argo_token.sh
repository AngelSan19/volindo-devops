#!/bin/bash
cluster_name=$1
environment=$2
zone=$3
# Ejecuta el comando kubectl para obtener el token de autenticación de Argo CD
token=$(kubectl get secret -n argocd argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 --decode)
# Actualiza la configuración de Kubectl en AWS EKS utilizando las variables "zone" y "cluster_name"
arn=$(aws eks --region $zone update-kubeconfig --name "${cluster_name}-${environment}" --query 'None' --output json)

# Crea un objeto JSON con el token como valor
output="{ \"token\": \"$token\" ,  \"arn-eks\": \"$arn\"}"

# Imprime el objeto JSON como salida
echo $output
