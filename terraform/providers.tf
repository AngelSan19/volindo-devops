terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0" #Update depend of the project
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0" #Update depend of the project
    }
    argocd = {
      source = "oboukili/argocd"
      version = "5.6.0" #Update depend of the project
    }
  }
}
