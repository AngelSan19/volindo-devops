# test-devops

This is a test repository for demonstration purposes showcasing how to deploy infrastructure on AWS using Terraform and the Argo CD tool with Helm integration.

## Description

This repository provides examples and sample files to guide you through the process of deploying infrastructure using Terraform and managing the application lifecycle with Argo CD. The repository includes two initial applications: `api-application` and `static-application`.

## Repository Structure

The repository is organized as follows:

- `terraform/`: Contains terraform files to deply infra on aws.
- `api-application/`: Contain manifest to deploy a api aplication(echoserver).
- `static-aplication/`: Contain manifest to deploy a static aplication(nginx).

## Requirements

Before using this repository, make sure you have the following requirements:

- [Terraform](https://terraform.io): Infrastructure provisioning tool.
- [aws-cli](https://aws.amazon.com/cli/) Tool conect with aws cloud
- [kubernetes](https://kubernetes.io/): Kubernetes tool

## Getting Started

To deploy the infrastructure and applications, follow these steps:

1. Clone this repository to your local machine.
2. Set up your AWS credentials and configure Terraform accordingly.
3. Update the Terraform variables in the appropriate files to match your environment.
4. Run `terraform init` to initialize the Terraform configuration.
5. Run `terraform plan` to preview the changes.
6. Run `terraform apply` to deploy the infrastructure and applications.

## Diagram infra
![infra.png](./media/infra-test-develop.png)

## Notes
all domains are CNAME and all point to the same balancer, for this it is necessary to create them and point to the balancer here is the example:
![aws-cert.png](./media/aws-cert.png)
![cname-aws-cert.png](./media/cname-aws-cert.png)
![cname-load-balancer.png](./media/cname-load-balancer.png)
## Contributing

If you want to contribute to this project, follow the steps below:

1. Fork this repository.
2. Make changes in your fork.
3. Create a pull request describing your changes.

## Contact

If you have any questions or suggestions related to this repository, feel free to contact the development team.

